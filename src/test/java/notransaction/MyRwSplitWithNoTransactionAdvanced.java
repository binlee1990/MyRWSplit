package notransaction;

import com.freedom.mysql.myrwsplit.bean.Role;
import com.freedom.mysql.myrwsplit.helper.LoggerHelper;
import com.freedom.mysql.myrwsplit.helper.MapperUtils;
import com.freedom.mysql.myrwsplit.interfaces.RoleMapper;

public class MyRwSplitWithNoTransactionAdvanced {
	@SuppressWarnings("unused")
	private static LoggerHelper LOGGER = LoggerHelper.getLogger(MyRwSplitWithNoTransactionAdvanced.class);

	// 在读写分离的情况下，必须重新获取mapper,才可以支持每次操作都重新判断来决定从master/slave来获取新的连接
	// 如果连续运行就会报错
	public static void main(String[] args) {
		//
		RoleMapper mapper = MapperUtils.getMapper(RoleMapper.class);
		Role role = mapper.getRole0(13);

		//
		mapper = MapperUtils.getMapper(RoleMapper.class);
		role = new Role();
		role.setAuthor("xxx");
		role.setTitle("yyy");
		mapper.insertRole(role);

	}
}
